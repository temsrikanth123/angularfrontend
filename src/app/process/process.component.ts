import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {
  @Input() public empProcessAreas: any;
  @Input() public employeeId: string;
  @Input() public processSubmitted: boolean;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }


  // tslint:disable-next-line:typedef
  submitProcess(form: NgForm) {
    // console.log(this.empProcessAreas);
    this.processSubmitted = true;
    localStorage.setItem('processSubmitted',String(this.processSubmitted));
  

    const requestBody = {
      empCertificationTaskId: localStorage.getItem('certificateId'),
      employeeId: localStorage.getItem('username'),
      empProcessAreas: this.empProcessAreas
    };

    console.log(requestBody);

    this.http.post('http://localhost:8080/certifying/save/processes', requestBody)
      .subscribe((response) => {
        console.log(response);
        this.empProcessAreas = response;
      });

  }

  // tslint:disable-next-line:typedef
  compliantChange(i: number, j: number, event: any) {
    this.empProcessAreas[i].empProcesses[j].isCompliant = event.target.value;
    console.log(event.target.value);
  }

  // tslint:disable-next-line:typedef
  severityChange(i: number, j: number, event: any) {
    this.empProcessAreas[i].empProcesses[j].severity = event.target.value;
    console.log(event.target.value);
  }

  // tslint:disable-next-line:typedef
  remarksChange(i: number, j: number, event: any) {
    this.empProcessAreas[i].empProcesses[j].remarks = event.target.value;
    console.log(event.target.value);
  }
}
