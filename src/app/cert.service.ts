import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CertificationTaskItem} from './certificates';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {
  private baseURL = "http://localhost:8080/coo/tasks";

  constructor(private httpClient: HttpClient) { }

  getCertificateList(): Observable<CertificationTaskItem[]>{
    return this.httpClient.get<CertificationTaskItem[]>(`${this.baseURL}`);
  }
}
