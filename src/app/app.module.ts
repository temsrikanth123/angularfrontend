import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { SignInComponent } from './sign-in.component';
import { FormsModule}  from '@angular/forms';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';

import { HomeComponent } from './home/home.component';

import { appRoutes } from './routes'
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth/auth.guard';
import { CertifierComponent } from './certifier/certifier.component';
import { DocumentsComponent } from './documents/documents.component';
import { RemarksComponent } from './remarks/remarks.component';
import { ProcessComponent } from './process/process.component';
import { MyCertificationComponent } from './my-certification/my-certification.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FooterComponent } from './footer/footer.component';
import { ClistComponent } from './clist/clist.component';
import { HeaderComponent } from './header/header.component';
import { FilterComponent } from './filter/filter.component';
import { LegendsComponent } from './legends/legends.component';
import { DigitalComponent } from './digital/digital.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { SuccessPopupComponent } from './success-popup/success-popup.component';
import { PreviewComponent } from './preview/preview.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HomeComponent,
    CertifierComponent,
    SignInComponent,
    ClistComponent,
    HeaderComponent,
    FilterComponent,
    LegendsComponent,
    routingComponents,
    MyCertificationComponent,
    FooterComponent,
    ProcessComponent,
    RemarksComponent,
    DocumentsComponent,
    SuccessPopupComponent,
    PreviewComponent,
    DigitalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatDialogModule
  ],
  entryComponents : [SuccessPopupComponent],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
