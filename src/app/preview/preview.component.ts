import { Component, OnInit, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { MatDialog } from '@angular/material/dialog';
import { SuccessPopupComponent } from '../success-popup/success-popup.component';
import { CertificationDetails } from '../app-interfaces';

@Injectable({
  providedIn: 'root'})

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  title = 'preview';
  popup=true;
  Response: CertificationDetails;
  certificatepdf;
  today: number =  Date.now();
  certificatefile;
  certificateId;
  employeeId;


  constructor(private http: HttpClient,public dialog: MatDialog ){}


    public convetToPDF()
    {
      let fileName = `${this.employeeId + " " + this.Response.certifier.businessUnitName + " " + this.Response.certifier.role + " " + this.Response.empCertTask.certTask.name }`;

        var data = document.getElementById('contentToConvert');
        html2canvas(data, {
          x:25,
          y:-550,
          windowHeight : 500,
          windowWidth : 1100
        }).then(canvas => {
        var imgWidth = 350;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        
        const contentDataURL = canvas.toDataURL('image/png')
        let pdf = new jspdf('p', 'px', 'a4'); // A4 size page of PDF
        var position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, 0, imgWidth, imgHeight)
        pdf.save(fileName);
        this.certificatefile = pdf.output('blob');
      });


      /*var data = document.getElementById('contentToConvert');
		html2canvas(data).then(canvas => {

	  let HTML_Width = canvas.width;
      let HTML_Height = canvas.height;
      let top_left_margin = 15;
      let PDF_Width = HTML_Width + (top_left_margin * 2);
      let PDF_Height = (PDF_Width * 4.0) + (top_left_margin * 2);
      let canvas_image_width = HTML_Width;
      let canvas_image_height = HTML_Height;
        
	 
		const contentDataURL = canvas.toDataURL('image/PNG')
		let pdf = new jspdf('p', 'mm', [PDF_Width, PDF_Height]);
		var position = 0;
		pdf.addImage(contentDataURL, 'PNG',top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
		pdf.save(fileName);
		});*/
    }



  // public convetToPDF()
  // {
  //   let fileName = `${this.employeeId + " " + this.Response.certifier.businessUnitName + " " + this.Response.certifier.role + " " + this.Response.empCertTask.certTask.name }`;

  //   let pdf = new jspdf('p', 'px', 'a1'); 
  //   pdf.html(document.getElementById("contentToConvert"), {
  //     x: 10,
  //     y: 10,
  //     callback: function (doc) {
  //       doc.save(fileName);
  //       // this.certificate = doc.output('blob');
  //     }
  //  });
  // }

   // var data = document.getElementById('contentToConvert');
    // html2canvas(data).then(canvas => {
    // var imgWidth = 208;
    // var imgHeight = canvas.height * (imgWidth / canvas.width);
    
    // const contentDataURL = canvas.toDataURL('image/png')
    // let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
    // var position = 0;
    // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
    // pdf.save(`${this.employeeId + " " + this.Response.certifier.businessUnitName + " " + this.Response.certifier.role + " " + this.Response.empCertTask.certTask.name }`);
    // this.certificatefile = pdf.output('blob');
  // });

  ngOnInit(): void {
    this.certificateId = localStorage.getItem('certificateId');
    this.employeeId = localStorage.getItem('username'); 
    
  const requestData={
      certificateId: this.certificateId,
      employeeId: this.employeeId
    }
    
   this.http.post<any>("http://localhost:8080/certifying/certificationTask/certificateId",requestData).
   subscribe((data:CertificationDetails) =>{
     this.Response=data;
     console.log(data)
    }
   )
  }
  
  response:any;
  fileToUpload:any;

  // var data = document.getElementById('contentToConvert');
    // html2canvas(data).then(canvas => {
    // var imgWidth = 208;
    // var pageHeight = 295;
    // var imgHeight = canvas.height * imgWidth / canvas.width;
    // var heightLeft = imgHeight;
    
    // const contentDataURL = canvas.toDataURL('image/png')
    // let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
    // var position = 0;
    // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, pageHeight)
    // this.certificatefile = pdf.output('blob');

    

  // submitProcessForm(){

  //   let fileName = `${this.employeeId + " " + this.Response.certifier.businessUnitName + " " + this.Response.certifier.role + " " + this.Response.empCertTask.certTask.name }`;

  //   var data = document.getElementById('contentToConvert');
  //   html2canvas(data).then(canvas => {
  //   var imgWidth = 208;
  //   var imgHeight = canvas.height * (imgWidth / canvas.width);
    
  //   const contentDataURL = canvas.toDataURL('image/png')
  //   let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
  //   var position = 0;
  //   pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
  //   // pdf.save(`${this.employeeId + " " + this.Response.certifier.businessUnitName + " " + this.Response.certifier.role + " " + this.Response.empCertTask.certTask.name }`);
  //   this.certificatefile = pdf.output('blob');
  // });

    // let pdf = new jspdf('p', 'px', 'a1'); 
    // let call;
    // const formData = new FormData();
        // formData.append("certificateTaskId",localStorage.getItem('certificateId'));
        // formData.append("empId",localStorage.getItem('username'));
        // formData.append("certificate",this.certificatefile);

        // console.log(formData.get("empId"));
        // console.log(formData.get("certificateTaskId"));
        // console.log(formData.get("certificate"))
        // call = this.http.post("http://localhost:8080/generate",formData);
        // this.onRecieve(call);

  //   let pdf = new jspdf('p', 'px', 'a1'); 
  //   let call;
    
  //   pdf.html(document.getElementById("contentToConvert"), {
  //     x: 10,
  //     y: 10,
  //     callback: function (doc) {
  //       this.certificatefile = doc.output('blob');

  //       const formData = new FormData();
  //       formData.append("certificateTaskId",localStorage.getItem('certificateId'));
  //       formData.append("empId",localStorage.getItem('username'));
  //       formData.append("certificate",this.certificatefile);

  //       console.log(formData.get("empId"));
  //       console.log(formData.get("certificateTaskId"));
  //       console.log(formData.get("certificate"))

        
  //       call = this.http.post("http://localhost:8080/generate",formData);
  //       this.onRecieve(call);
  //     }
  //  });   
  // }

  submitProcessForm(){

    const formData = new FormData();
    formData.append("certificateTaskId",this.certificateId);
    formData.append("empId",this.employeeId);
    formData.append("certificate",this.certificatefile);

    console.log(formData)
    const call=this.http.post("http://localhost:8080/generate",formData);

    call.subscribe((response)=>{
      this.response=response;
      this.onSubmit();
      console.log(response)
    })
  }


  onRecieve(call) {
    call.subscribe((response) => {
      this.response=response;
      this.onSubmit();
      console.log(response)
    },
    (error) => {
      console.log(error);
    });
  }
  
  onSubmit(){
    const dialogRef = this.dialog.open(SuccessPopupComponent,
      {
        width : '400px'
      });
  }
}
