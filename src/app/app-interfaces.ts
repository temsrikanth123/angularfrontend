export interface CertificationDetails {
    certifier: Certifier;
    reporter: Reporter;
    empCertTask: EmployeeCertificationTask;
  }
  
  export interface Certifier {
    employeeName: string;
    role: string;
    businessUnitName: string;
  }
  
  export interface Reporter {
    nameOfReporter: string;
    reporterRole: string;
    parentBusinessUnitName: string;
  }
  
  export interface EmployeeCertificationTask{
    dueDate: Date;
    certifiedOn: Date;
    certifierName: string;
    certTask: CertificationTask;
    savedDetails: SavedDetails;
    processSubmitted: boolean;
    remarksSubmitted: boolean;
  }
  
  export interface CertificationTask {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
    plannedStartDate: Date;
  }
  
  export interface SavedDetails {
    id: number;
    isCompliant: string;
    certificationStatus: string;
    empProcessAreas: EmpProcessArea[];
    remarks: Remark[];
    additionalDocuments: AdditionalDocument[];
    certificate: Certificate[];
    additionalDocumentsSubmitted: boolean;
  }
  
  export interface EmpProcessArea {
    id: number;
    processAreaId: number;
    employeeCertificationTaskId: number;
    empProcesses: EmpProcess[];
  }
  
  export interface EmpProcess {
    id: number;
    process: Process;
    isCompliant: string;
    severity: string;
    remarks: string;
  }
  
  export interface Process {
    processId: number;
    name: string;
  }
  
  export interface Remark {
    id: number;
    serialNumber: number;
    empCertificationTaskId: number;
    empAssetDetailsId: string;
    isCompliant: string;
    remarks: string;
  }
  
  export interface AdditionalDocument {
    id: number;
    fileName: string;
    empCertificationTaskDetailsId: number;
    path: string;
    description: string;
  }
  
  export interface Certificate {
    path: string;
    description: string;
    empCertificationTaskDetailsId: number;
  }
  