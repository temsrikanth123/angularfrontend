import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReportResponse } from './coo';

@Injectable({
  providedIn: 'root'
})
export class CooService {

  private baseURL = "http://localhost:8080/view_report/view";


  constructor(private httpClient: HttpClient) {  }
    getCertifier(): Observable<ReportResponse[]>{
      return this.httpClient.get<ReportResponse[]>(`${this.baseURL}`);
    }
    
 
}
