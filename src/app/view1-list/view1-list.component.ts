import { Component, OnInit } from '@angular/core';
import { ReportResponse } from '../coo';
import { CooService } from '../coo.service';
import { CertificateService } from '../cert.service';
import {CertificationTaskItem } from '../certificates';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export class tableemp{
  constructor(
    public taskId: number,
    public taskName: string,
    public startDate: string,
    public endDate: string,
    public PlannedStartDate: string,
    public dueDate: string,
    public certifiedOn: string,
    public status: string,

  ){}
}


@Component({
  selector: 'app-view1-list',
  templateUrl: './view1-list.component.html',
  styles: [
  ]
})
export class View1ListComponent implements OnInit {
  searchCertifier: any;
  
 constructor(private rs: CooService, private rs1: CertificateService, private http: HttpClient) { }
  columns = ["Business unit" , "Certifier" ,"Due Date", "Certified on date" , "Non-Complaince", "Status","View Certificate"];

  coolandings : ReportResponse[] = [];
  certificate : CertificationTaskItem[] = [];
  citi: tableemp[] | undefined;

  username: string;
  businessUnitName: string;
  certificationTaskName: string;
  startDate: string;
  endDate: string;
  businessUnitListOneLevelBelow: Array<string>;
  showClose:string;
  role:string;

  ngOnInit(): void {
    this.showClose = localStorage.getItem('showClose');
    this.username = localStorage.getItem('username');
    this.businessUnitName = localStorage.getItem('businessUnitName');
    this.certificationTaskName = localStorage.getItem('certificationTaskName');
    this.role = localStorage.getItem('role');
    this.startDate = localStorage.getItem('startDate');
    this.endDate = localStorage.getItem('endDate');

    this.getReportingStatusForAll()
      
    this.posttab();
    this.getListOfBusinessUnitOneLevelBelow();
      
  }

  getReportingStatusForAll() {
    const requestData={
      empId:this.username,
      businessUnitName:this.businessUnitName,
      certificationTaskName:this.certificationTaskName,
    }
    
    this.http.post<any>("http://localhost:8080/view_report/view",requestData).subscribe
      (
        (Response)=>
        {
          this.coolandings = Response;
        },
        (error)=>
        {
          console.log("error occured :" +error);
        }
      )
      this.rs1.getCertificateList().subscribe
      (
        (Response)=>
        {
          this.certificate = Response;
        },
        (error)=>
        {
          console.log("error occured :" +error);
        }
      )

  }

  getListOfBusinessUnitOneLevelBelow() {
    const requestBody = {
      businessUnitName : this.businessUnitName
    }
    this.http.post('http://localhost:8080/view_report/list', requestBody)
    .subscribe((response: Array<string>) => {
      this.businessUnitListOneLevelBelow = response;
    })
  }


  onBusinesssUnitNameSelected(businessUnitName: string) {
    if(businessUnitName === "All") {
      this.getReportingStatusForAll();
    } else {
      const requestBody = {
        employeeId:this.username,
        nameOfCertificationTask: this.certificationTaskName,
        businessUnitName: businessUnitName
      }
      this.http.post('http://localhost:8080/view_report/businessUnit',requestBody)
      .subscribe((response: ReportResponse[]) => {
        this.coolandings = response;
      })
    }
  }

  onCertificateView(id:number) {
    this.http.get(`http://localhost:8080/coo/view/certificate/${id}`, {
      responseType:"blob"
     }).subscribe((response) => {
      console.log("Got File ");
      this.downloadFile(response);
    })    
  }

  downloadFile(data: any) {
    const blob = new Blob([data], { type: 'text/csv' });
    const url= window.URL.createObjectURL(blob);
    // window.open(url,"_blank");
    var link = document.createElement('a');
    link.href = url;
    // link.download="file.pdf";
    link.target="_blank";
    link.click();
  }

  posttab(){
    this.http.post<any>(`http://localhost:8080/certification_task/tasks?employeeId=${localStorage.getItem('username')}`, null)
    .subscribe((response) => {
      console.log(response);
      this.citi = response;
    }
    );
  }
}