import { ComponentFixture, TestBed } from '@angular/core/testing';

import { View1ListComponent } from './view1-list.component';

describe('View1ListComponent', () => {
  let component: View1ListComponent;
  let fixture: ComponentFixture<View1ListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ View1ListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(View1ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
