import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

export class tableemp{
  constructor(
    public taskId: number,
    public taskName: string,
    public startDate: string,
    public endDate: string,
    public PlannedStartDate: string,
    public dueDate: string,
    public certifiedOn: string,
    public status: string,

  ){}
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  
  constructor(private http: HttpClient, private router: Router) { }

 citi: tableemp[] | undefined;
  username: any;

  ngOnInit(): void {
    console.log(localStorage.getItem('name'));
    this.username = localStorage.getItem('name');
    this.posttab();
  }
  Logout() {
    localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }

  posttab(){
    this.http.post<any>(`http://localhost:8080/certification_task/tasks?employeeId=${localStorage.getItem('username')}`, null)
    .subscribe((response) => {
      console.log(response);
      this.citi = response;
    }
    )};

  }


