import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReportResponse } from './coo';

@Injectable({
  providedIn: 'root'
})
export class Coo2Service {

  private baseURL = "http://localhost:8080/coocert/10";


  constructor(private httpClient: HttpClient) {  }
    getCertifier(): Observable<ReportResponse[]>{
      return this.httpClient.get<ReportResponse[]>(`${this.baseURL}`);
    }
    
 
}
