import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { View1ListComponent } from './view1-list/view1-list.component'


const routes: Routes = [
  { path: 'view13', component: View1ListComponent }
  // { path: 'view14', component: View2ListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [View1ListComponent]
