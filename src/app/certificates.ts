/*export class Certificates {
Task: String;
CompailancePeriod: String;
Action: String;
constructor(Task: String, CompailancePeriod: String, Action: String)
{
  this.Task = Task;
  this.CompailancePeriod = CompailancePeriod;
  this.Action = Action;
}
}*/
/*export class Certificates {
  id: number;
  end_date: Date;
  name: String;
  planned_start_date: Date;
  start_date: Date;
  constructor(id: number, end_date: Date, name: String, planned_start_date: Date, start_date: Date) {
    this.id = id;
    this.end_date = end_date;
    this.name = name;
    this.planned_start_date = planned_start_date;
    this.start_date = start_date;
  }
}*/



export class CertificationTaskItem{
  certificateTaskId: number;
  taskName: string;
  startDate: Date;
  endDate: Date;
  constructor(certificateTaskId : number, taskName : string, startDate: Date, endDate: Date){
    this.certificateTaskId = certificateTaskId;
    this.taskName = taskName;
    this.startDate = startDate;
    this.endDate = endDate;
  }
}
