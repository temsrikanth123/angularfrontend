import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient, HttpParams} from '@angular/common/http';
import { AdditionalDocument } from '../app-interfaces';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  @Input() additionalDocuments: AdditionalDocument[];
  @Input() private employeeId: string;
  @Input() private employeeCertificationTaskSavedDetailId: number;
  @Input() additionalDocumentsSubmitted: boolean;

  private documentToUpload: any;
  private description: string;


  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  additionalDocumentSubmit(form: NgForm) {
    
    const formData = new FormData();
    formData.append('file', this.documentToUpload);
    formData.append('description', this.description);
    formData.append('employeeId', localStorage.getItem('username'));
    // @ts-ignore
    formData.append('employeeCertificationTaskSavedDetailId', this.employeeCertificationTaskSavedDetailId);

    console.log(this.documentToUpload)
    console.log(this.description)
    console.log(this.employeeCertificationTaskSavedDetailId)

    form.controls.description.setValue('');
    form.controls.file.setValue('')

    console.log(formData)

    this.http.post('http://localhost:8080/additional_document', formData)
      .subscribe((response: AdditionalDocument) => {
        console.log(response);
        this.additionalDocuments.push(response);
      });
  }

  // tslint:disable-next-line:typedef
  documentChange(fileElement: any) {
    // console.log(fileElement);
    this.documentToUpload = fileElement;
  }

  // tslint:disable-next-line:typedef
  descriptionChange(description: string) {
    console.log(description);
    this.description = description;
  }

  // tslint:disable-next-line:typedef
  onRemoveClick(i: number, $event: any) {
    const id = this.additionalDocuments[i].id;
    const requestBody = {
      additionalDetailsId: id,
      empCertificationTaskDetailsId: this.employeeCertificationTaskSavedDetailId,
      employeeId: this.employeeId,
    };
    this.http.post('http://localhost:8080/additional_document/delete', requestBody)
      .subscribe((response: AdditionalDocument[]) => {
        console.log(response);
        this.additionalDocuments = response;
      });
  }

  // tslint:disable-next-line:typedef
  confirmDocuments() {
    const requestBody = {
      empCertificationTaskDetailsId : this.employeeCertificationTaskSavedDetailId,
      submit: true
    };
    this.http.post('http://localhost:8080/additional_document/submitted', requestBody)
      .subscribe((response: boolean) => {
        this.additionalDocumentsSubmitted = response;
      });
  }

}
