import { TestBed } from '@angular/core/testing';

import { Coo2Service } from './coo2.service';

describe('Coo2Service', () => {
  let service: Coo2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Coo2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
