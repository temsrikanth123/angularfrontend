import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { CertificationDetails, Certifier, EmployeeCertificationTask, Reporter } from '../app-interfaces';



@Component({
  selector: 'app-my-certification',
  templateUrl: './my-certification.component.html',
  styleUrls: ['./my-certification.component.css']
})
export class MyCertificationComponent implements OnInit {

  public certifier: Certifier;
  public reporter: Reporter;
  public empCertTask: EmployeeCertificationTask;
  public proc=true;
  public rem=false;
  public addi=false;
  public visibility=false;
  public popup=false
  employeeCertificationTaskSavedDetailId: number;
  employeeId: string;

  dontShow = ["Ip Asset Owner","Sub-ISU Head","Sub-SP Head"]
  showReporting = "Yes";

  
  
  certificateId: number;
  response: any;
  processSubmitted = false;
  remarksSubmitted = false;

  constructor(private http: HttpClient) { }
    
  ngOnInit(): void {

    this.certificateId = Number(localStorage.getItem('certificateId'));

    localStorage.setItem('processSubmitted',String(false));

    const requestObject = {
      certificateId: localStorage.getItem('certificateId'),
      employeeId: localStorage.getItem('username')
    };

    const certifierCertificationDetails = this.http.post('http://localhost:8080/certifying/certificationTask/certificateId', requestObject);

    certifierCertificationDetails.subscribe((response: CertificationDetails) => {
      console.log(response);
      this.response = response;
      this.certifier = response.certifier;
      this.reporter = response.reporter;
      this.empCertTask = response.empCertTask;
      this.employeeCertificationTaskSavedDetailId = response.empCertTask.savedDetails.id; 
      this.employeeId = localStorage.getItem('username')

      this.processSubmitted = response.empCertTask.processSubmitted;
      this.remarksSubmitted = response.empCertTask.remarksSubmitted;

      localStorage.setItem('processSubmitted',String(this.processSubmitted));
      console.log(localStorage.getItem('processSubmitted'));
      
      localStorage.setItem('businessUnitName', response.certifier.businessUnitName);
      localStorage.setItem('certificationTaskName',response.empCertTask.certTask.name);

      localStorage.setItem('startDate',String(response.empCertTask.certTask.startDate));
      localStorage.setItem('endDate',String(response.empCertTask.certTask.endDate));

      localStorage.setItem("showClose","");
      localStorage.setItem('role',response.certifier.role);

      for(let i = 0 ; i < this.dontShow.length ; i++) {
        if (this.dontShow[i] === response.certifier.role) {
          this.showReporting = "No";
        }
      }
    });
  }

  closePopUp() {
    this.popup = false;
  }

  actProcess(){
    this.rem=false;
    this.addi=false;
    this.proc=true;
    this.visibility=true
  }
  actRemarks(){
    this.proc=false;
    this.addi=false;
    this.rem=true;
    this.visibility=true; 
  }
  actAdditional(){
    this.proc=false;
    this.rem=false;
    this.addi=true;
    this.visibility=true;
  }
  myFunction(){
    this.proc=false;
    this.rem=false;
    this.addi=false;
    this.visibility=false;
  }

  popUpState = false;
  changePopUpState() :void {
    if(this.popUpState)
      this.popUpState = false;
    else
      this.popUpState = true;

  }


}


