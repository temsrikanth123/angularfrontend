import { Component, OnInit } from '@angular/core';
import { CertificationTaskItem } from '../certificates';
import { CertificateService } from '../cert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clist',
  templateUrl: './clist.component.html',
  styleUrls: ['./clist.component.css']
})
export class ClistComponent implements OnInit 
{

  /*certificate: Certificates[] = [];*/
    searchTask: any;
    cooo: boolean = false;
  constructor(private rs: CertificateService, private router: Router){}

  columns = ["Task" , "Compliance Period" ,"Action"];
  certificate: CertificationTaskItem[] = [];
  filteredCertificates: CertificationTaskItem[] = [];
  

  businessUnitName: string;
  showView=false;

  onViewClick(taskName,startDate,endDate) {
    localStorage.setItem('certificationTaskName',taskName);
    localStorage.setItem('startDate',startDate);
    localStorage.setItem('endDate',endDate);
    localStorage.setItem("showClose","Show");
    localStorage.setItem('role','COO');
    this.cooo = true;
  }

  ngOnInit(): void {
    this.businessUnitName =  'TCS-GLOBAL';
    localStorage.setItem("businessUnitName",this.businessUnitName);

    this.rs.getCertificateList().subscribe
      (
        (Response)=>
        {
          this.certificate = this.filteredCertificates = Response;
        },

        (error)=>
        {
          console.log("error occured :" +error);
        }
      )
  }

  getFilteredCert(id:any){
    let filteredCert: CertificationTaskItem[] = []
    if(id === '0'){
      this.filteredCertificates = this.certificate;
    }
    else{
      for(let i in this.certificate){
        if(this.certificate[i].certificateTaskId.toString() === id){
          console.log(id);
          filteredCert.push(this.certificate[i]);
        }
      }
      this.filteredCertificates = filteredCert;
    } 
  }  
}
