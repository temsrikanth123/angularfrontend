/*export class CooLandings 
 {
    cername: string;
    name: string;
    certifier_name: string;
    certified_on: Date;
    due_date: Date;
    is_compliant: string;
    certification_status: string;
    path: String;
    constructor(cername: string, name: string, certifier_name: string, certified_on: Date, due_date: Date, is_compliant: string, certification_status: string, path: string )
    {
        this.cername = cername;
        this.name = name;
        this.certifier_name = certifier_name;
        this.certified_on = certified_on;
        this.due_date = due_date;
        this.is_compliant = is_compliant;
        this.certification_status = certification_status;
        this.path = path;
    }
}*/

export class ReportResponse{
    businessUnitName: string;
    certifierName: string;
    dueDate: Date;
    certifiedOnDate: Date;
    compliant: boolean;
    certificationStatus: string;
    certificateUrl: string;
    
    constructor(businessUnitName: string, certifierName: string, dueDate : Date, certifiedOnDate: Date, compliant: boolean,certificationStatus: string, certificateUrl: string )
    {
        this.businessUnitName = businessUnitName;
        this.certifierName = certifierName;
        this.dueDate = dueDate;
        this.certifiedOnDate = certifiedOnDate;
        this.compliant = compliant;
        this.certificationStatus = certificationStatus;
        this.certificateUrl = certificateUrl;
    }

}
