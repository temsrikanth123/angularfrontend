import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import { Remark } from '../app-interfaces';

@Component({
  selector: 'app-remarks',
  templateUrl: './remarks.component.html',
  styleUrls: ['./remarks.component.css']
})
export class RemarksComponent implements OnInit {
  @Input() remarks: Remark[];
  @Input() employeeId: string;
  @Input() remarksSubmitted: boolean;

  constructor(private http: HttpClient) { 
    // console.log(this.remarks);
  }

  ngOnInit(): void {
    // console.log(this.remarks);
  }

  // tslint:disable-next-line:typedef
  remarkChange(i: number, event: any) {
    this.remarks[i].remarks = event.target.value;
  }

  // tslint:disable-next-line:typedef
  complianceChange(i: number, event: any) {
    this.remarks[i].isCompliant = event.target.value;
  }


  // tslint:disable-next-line:typedef
  submitRemark(form: NgForm) {
    // TODO : block this component if remarks not present
    // this.remarks.length > 0 ?
    this.remarksSubmitted = true;
    const requestBody = {
      employeeId: localStorage.getItem('username'),
      empCertificationTaskId: localStorage.getItem('certificateId'),
      remarks: this.remarks
    };

    console.log(requestBody);

    this.http.post('http://localhost:8080/certifying/save/remarks', requestBody).subscribe((response: Remark[]) => {
      this.remarks = response;
      console.log(response);
    });
  }
}
