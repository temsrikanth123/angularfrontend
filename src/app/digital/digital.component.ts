import { Component, Input, ViewChild, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-digital',
  templateUrl: './digital.component.html',
  styleUrls: ['./digital.component.css']
})
export class DigitalComponent implements OnInit {

  @Input() name: string;
  @ViewChild('sigPad') sigPad;
  sigPadElement;
  context;
  isDrawing = false;
  img;
  hide;
  hideimage=true;

  ngOnInit() {
    this.sigPadElement = this.sigPad ? this.sigPad['nativeElement'] : null;
    this.context = this.sigPad ? this.sigPadElement.getContext('2d') : null;
    if(this.context){
      this.context.strokeStyle = '#3742fa';
    }
  }


  @HostListener('document:mouseup', ['$event'])
  onMouseUp(e) {
    this.isDrawing = false;
  }

  onMouseDown(e) {
    this.isDrawing = true;
    const coords = this.relativeCoords(e);
    if(this.context){
      this.context.moveTo(coords.x, coords.y);
    }
  }

  onMouseMove(e) {
    if (this.isDrawing && this.context) {
      const coords = this.relativeCoords(e);
      this.context.lineTo(coords.x, coords.y);
      this.context.stroke();
    }
  }

  private relativeCoords(event) {
    const bounds = event.target.getBoundingClientRect();
    const x = event.clientX - bounds.left;
    const y = event.clientY - bounds.top;
    return { x: x, y: y };
  }

  clear() {
    this.context.clearRect(0, 0, this.sigPadElement.width, this.sigPadElement.height);
    this.context.beginPath();
  }

  save() {
    this.img = this.sigPadElement.toDataURL("image/png");
    console.log(this.img);
  }
  
  toHide()
{
  this.hide=true;
  this.hideimage=false;
}

}
