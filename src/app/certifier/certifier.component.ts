import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

export class tableemp{
  constructor(
    public taskId: number,
    public taskName: string,
    public startDate: string,
    public endDate: string,
    public PlannedStartDate: string,
    public dueDate: string,
    public certifiedOn: string,
    public status: string,

  ){}
}
@Component({
  selector: 'app-certifier',
  templateUrl: './certifier.component.html',
  styleUrls: ['./certifier.component.css']
})
export class CertifierComponent implements OnInit {
  // citi: tableemp[] | undefined;  
  citi:any;
  certTask;
  username: any;
  initialValue=-1;

  constructor(private http: HttpClient, private router: Router) { }


  filterSubmit(form:NgForm) {
    console.log(form.value)
    if(form.value.certificateName<0) {
      this.posttab();
    } else {
      const requestBody = {
        "employeeId": localStorage.getItem("username"),
        "certificationTaskId": form.value.certificateName
      }

      this.http.post("http://localhost:8080/certification_task/tasks/filter/certificate_name",requestBody).subscribe((response) => {
        this.citi = response;
      });
    }
  }

  ngOnInit() {
    // console.log(localStorage.getItem('name'));
    this.username = localStorage.getItem('name');
    this.posttab();
    this.fetchCertificationTaskForFilter();    
  }

  fetchCertificationTaskForFilter() {
    this.http.get("http://localhost:8080/certification_task/all").subscribe(response => {
      this.certTask = response;
      console.log(response)
    })
  }

  posttab(){
    this.http.post<any>(`http://localhost:8080/certification_task/tasks?employeeId=${localStorage.getItem('username')}`, null)
    .subscribe((response) => {
      // console.log(response);
      this.citi = response;
    }
    );
  }
  next(id: string) {
    localStorage.setItem('certificateId', id);
    this.router.navigate(['/mycertification']);
  }
  Logout() {
    localStorage.removeItem('password');
    this.router.navigate(['/login']);
  }

 
  filter: any;
  request = [{ task: 'IP & E certificate', compliance: '01-Oct-2020 to 31-dec-2020', plannedStartDate: '8-Jan-2020', dueDate: '8-Jan-2020', certificateOnDate: '', action: 'certify', status: 'pending' },
  { task: 'Contractual Compliance certificate', compliance: '01-Oct-2020 to 31-dec-2020', plannedStartDate: '8-Jan-2020', dueDate: '8-Jan-2020', certificateOnDate: '', action: 'certify', status: 'pending' }]

}

export interface CertificationTask {
  certificationTaskId: number;
  certificationTaskName: string;
}
