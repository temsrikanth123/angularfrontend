import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  isLoginError: boolean = false;
  isCOO:boolean=false;
 
 successLogin;
   user={
     employeeId:'',
     password:''
   }
   constructor(private http : HttpClient, private router : Router) { }
   loginUser(){
   const body = {
         employeeId:this.user.employeeId,
         password:this.user.password
       }

       const call = this.http.post("http://localhost:8080/employee/login",body);

       call.subscribe((response) => {
         console.log( response)
         this.successLogin = response
         if (response['exists'] &&  response['role']!="COO") {
           localStorage.setItem('name', response['username']);
           localStorage.setItem('username',this.user.employeeId);
           this.router.navigate(['/certifier']);
         }else if(!response['exists']) {
             this.isLoginError = true;
         }else if(response['role']=="COO"){
          console.log("COO");
          localStorage.setItem('name', response['username']);
          localStorage.setItem('username',this.user.employeeId);
          this.router.navigate(['/clist']);
       }
       })
     
   }

}
