import { Routes } from '@angular/router'
import { AuthGuard } from './auth/auth.guard';
import { CertifierComponent } from './certifier/certifier.component';
import { DocumentsComponent } from './documents/documents.component';
import { HomeComponent } from './home/home.component';
import { MyCertificationComponent } from './my-certification/my-certification.component';
import { ProcessComponent } from './process/process.component';
import { RemarksComponent } from './remarks/remarks.component';
import { SignInComponent } from './sign-in.component';
import { UserComponent } from './user/user.component';
import { ClistComponent } from './clist/clist.component';
import { HeaderComponent } from './header/header.component';
import { FilterComponent } from './filter/filter.component';
import { LegendsComponent } from './legends/legends.component';
import { FooterComponent } from './footer/footer.component';
import { View1ListComponent } from './view1-list/view1-list.component';




export const appRoutes: Routes = [
  { path: 'view13', component: View1ListComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'clist', component: ClistComponent, canActivate: [AuthGuard] },
  { path: 'filter', component: FilterComponent, canActivate: [AuthGuard] },
  { path: 'footer', component: FooterComponent, canActivate: [AuthGuard] },
  { path: 'header', component: HeaderComponent, canActivate: [AuthGuard] },
  { path: 'legends', component: LegendsComponent, canActivate: [AuthGuard] },
  { path: 'view1-list', component: View1ListComponent, canActivate: [AuthGuard] },
  { path: 'certifier', component: CertifierComponent, canActivate: [AuthGuard] },
  { path: 'mycertification', component: MyCertificationComponent, canActivate: [AuthGuard] },
  { path: 'documents', component: DocumentsComponent, canActivate: [AuthGuard] },
  { path: 'process', component: ProcessComponent, canActivate: [AuthGuard] },
  { path: 'remarks', component: RemarksComponent, canActivate: [AuthGuard] },
 { path: 'login', component: UserComponent,
children: [{ path: '', component: SignInComponent }]
},
{path: '', redirectTo:'/login',pathMatch : 'full'}


];
